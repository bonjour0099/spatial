const firebaseConfig = {
    apiKey: "AIzaSyB8cO4p-IM8D5klx6CJSDP49Ux5TOp0AyQ",
    authDomain: "contact-form-8ab73.firebaseapp.com",
    databaseURL: "https://contact-form-8ab73-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "contact-form-8ab73",
    storageBucket: "contact-form-8ab73.appspot.com",
    messagingSenderId: "844813200852",
    appId: "1:844813200852:web:1128e427ba931ca5523a41",
    measurementId: "G-E5L5W1080K"
};
firebase.initializeApp(firebaseConfig);
var contactFormDB = firebase.database().ref("contact-form");

document.getElementById("contactForm").addEventListener("submit", submitForm);

function submitForm(e) {
  e.preventDefault();

  var name = getElementVal("name");
  var emailid = getElementVal("emailid");
  var msgContent = getElementVal("msgContent");
  saveMessages(name, emailid, msgContent);

  //   enable alert
  document.querySelector(".alert").style.display = "block";

  //   remove the alert
  setTimeout(() => {
    document.querySelector(".alert").style.display = "none";
  }, 3000);

  //   reset the form
  document.getElementById("contactForm").reset();
}

const saveMessages = (name, emailid, msgContent) => {
  var newContactForm = contactFormDB.push();

  newContactForm.set({
    name: name,
    emailid: emailid,
    msgContent: msgContent,
  });
};

const getElementVal = (id) => {
  return document.getElementById(id).value;
};

